#  ***SCALING*** 

# **Abstract**
 
A project is going through some performance and scaling issues. After some analysis, the team lead has asked to investigate the possibility of using load balancers and understand vertical/horizontal scaling solutions.

#  **Introduction**
 
The project has to be scaled without affecting its performance, rather increasing it. The solution can be founding only after knowing what is scalability. Hence the concepts of Scalability, Vertical & Horizontal Scaling & Load Balancing with its algorithms are discussed in this paper.
 
## Scalability
 
The ability of a system to perform well when a large number of users increases is known as Scalability. It is a crucial part of system design and can be achieved in various ways such as vertical or horizontal scaling, load balancing, caching, etc.
 
 
## Vertical Scaling
Vertical scaling increases the scale of a system by adding more power to the existing machine. In practice, this would mean adding better processors, increasing RAM, or other power-increasing adjustments.
 
The drawback of Vertical Scaling is that hardware upgrades have limitations & are expensive.
 
## Horizontal Scaling
Horizontal scaling increases the scale of a system by adding more machines. This can be done when need and hence is flexible. A special program or interface is needed to manage the request and data on the distributed systems. This program is known as the Load balancer.
 
## Comparing Horizontal & Vertical Scaling
 
Vertical Scaling | Horizontal Scaling
------------ | -------------
Limited Scaling due to hardware limits. | Scales well as any number of systems can be added.
Data is consistent being on the same system. | Data is inconsistent being distributed on systems.
Single point failure, high risk. | Resilient to fail as many systems to serve.
Fast due to inter-process communication. | Can be slow due to network calls.
No additional program is required. | Load Balancer is required for distribution.
 
 
## Load Balancing
The program used to distribute the requests (i.e load) of a project to different systems while horizontal scaling is known as Load Balancer. It can be a simple program that keeps alternating with the system to send requests or a DNS server routing the requests to different IP addresses of the servers. There are various algorithms used for load Balancing, some are discussed below.
 
## Algorithms
* ###  Round Robin
  
   The most simple and most used algorithm of load balancing is Round Robin. Every new request is circularly sent to the next server. This algorithm divides the load evenly regardless of the specifications of the server or the number of active connections on the server. It is good if all the servers are of the same specifications & the number of active connections doesn’t matter.
 
   ![Round-Robin](images/round-robin_algorithm1.png)
 
 
* ###  Weighted Round Robin
 
   The Weighted Round Robin algorithm solves the problem of servers with different specifications by giving them weights proportional to their capacity. Thus load is distributed according to the capacity of the servers. The problem of a busy server still exists.
 
   ![weighted-Round-Robin](https://www.jscape.com/hubfs/images/weighted_round_robin.png)
  
 
* ###  Least Connections
 
   The Least Connections algorithm solves the problem of a busy server by sending requests to the server with the least active connections. This algorithm only considers the number of active connections on a server & not the capacity of the server.
  
   ![Least Connections](https://www.jscape.com/hubfs/images/least_connections_algorithm_solves_this.png)
  
 
* ###  Weighted Least Connections
  
   The Weighted Least Connections algorithm solves both the problems of different specifications servers as well as a busy server. This algorithm routes the request according to the weight assigned and the number of active connections to a server.
   This algorithm may seem the best suited for all cases but it comes with the extra cost of always tracking the number of active connections on all servers as well as storing the weights of all the servers. Hence should be used only when required.
 
 
* ### Random
 
   As the name suggests, the Random Load balancer routes request randomly to a server. It uses a random number generator to decide which server to choose.
   In cases wherein the load balancer receives a large number of requests, a Random algorithm will be able to distribute the requests evenly to the server. It is only good when all servers have the same specifications.
 
   ![Random](images/random.png)
 
# **Conclusion**
 
As the given task was to investigate scalability using load balancers, the project can be scaled horizontally using the best fit load balancing algorithm required for the project & if a new system is to be added the performance can be increased vertically if it fits the project budget. Also if there is a timely rush of requests, the project can be scaled horizontally on rented servers for the time being.
 
# **References**
Structure of Technical Paper
  
* [Typical structural layout](http://home.iitk.ac.in/~dasgupta/teaching/ComSkill/PapStruct/AppA.html)
 
*   [Markdown Template](https://github.com/pankajkarman/paper-template)
 
[Sacalabity](https://youtu.be/-W9F__D3oY4)
 
[Vertical & Horizontal Scaling](https://medium.com/@spencerwgoodman/vertical-and-horizontal-scaling-in-system-design-718cabb80a1a)
 
[Load Balancing Algorithms](https://www.jscape.com/blog/load-balancing-algorithms)
